module gitlab.com/mrvik/go-static-server

go 1.19

require (
	github.com/sirupsen/logrus v1.9.0
	golang.org/x/sys v0.0.0-20220928140112-f11e5e49a4ec
)
