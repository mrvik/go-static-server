package main

import (
	"net/http"
	"path"
	"time"

	"github.com/sirupsen/logrus"
)

type queryLogger struct {
	log     *logrus.Logger
	backend http.Handler
}

func newQueryLogger(level logrus.Level, back http.Handler) queryLogger {
	log := logrus.New()
	log.SetLevel(level)

	return queryLogger{
		log:     log,
		backend: back,
	}
}

func (l queryLogger) ServeHTTP(rw http.ResponseWriter, req *http.Request) {
	start := time.Now()
	codeMid := codeSniffer{
		rw,
		http.StatusOK,
	}

	l.backend.ServeHTTP(&codeMid, req)
	l.log.WithFields(logrus.Fields{
		"duration": time.Since(start).Milliseconds(),
		"path":     req.URL.Path,
		"status":   codeMid.int,
	}).Infof("Serving %q", path.Base(req.URL.Path))
}

type codeSniffer struct {
	http.ResponseWriter
	int
}

func (s *codeSniffer) WriteHeader(code int) {
	s.int = code
	s.ResponseWriter.WriteHeader(code)
}
