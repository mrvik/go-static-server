package main

import (
	"errors"
	"io/fs"
	"net/http"
)

type DfFileServer struct {
	base        http.FileSystem
	defaultFile string
}

func createFileHandler(base, defaultFile string) DfFileServer {
	dirfs := http.Dir(base)

	return DfFileServer{
		base:        dirfs,
		defaultFile: defaultFile,
	}
}

func (d DfFileServer) Open(filename string) (http.File, error) {
	file, err := d.base.Open(filename)
	if err == nil || !errors.Is(err, fs.ErrNotExist) {
		return file, err
	}

	return d.base.Open(d.defaultFile)
}
