package main

import (
	"context"
	"errors"
	"flag"
	"net"
	"net/http"
	"os/signal"
	"time"

	"github.com/sirupsen/logrus"
	"golang.org/x/sys/unix"
)

const MaxTimeout = time.Second * 10

func main() {
	var (
		listenAddr  string
		defaultFile string
		baseDir     string
		doLogAccess bool
	)

	flag.StringVar(&baseDir, "base", "/app", "Base directory to be served")
	flag.StringVar(&listenAddr, "addr", ":8080", "Listen address")
	flag.StringVar(&defaultFile, "default-filename", "index.html", "Default filename to be served")
	flag.BoolVar(&doLogAccess, "log-access", false, "Log accesses")
	flag.Parse()

	signalContext, cancel := signal.NotifyContext(context.Background(), unix.SIGINT, unix.SIGTERM)
	defer cancel()

	handler := http.FileServer(createFileHandler(baseDir, defaultFile))
	if doLogAccess {
		handler = newQueryLogger(logrus.InfoLevel, handler)
	}

	server := &http.Server{ //nolint: exhaustivestruct,exhaustruct
		Addr:              listenAddr,
		Handler:           handler,
		ReadHeaderTimeout: MaxTimeout,
		BaseContext:       func(net.Listener) context.Context { return signalContext },
	}

	go func() {
		<-signalContext.Done()

		ctx, end := context.WithTimeout(context.Background(), time.Second*10)
		defer end()

		server.Shutdown(ctx)

		if ctx.Err() != nil {
			server.Close()
		}
	}()

	if err := server.ListenAndServe(); err != nil && !errors.Is(err, http.ErrServerClosed) {
		panic(err.Error())
	}
}
